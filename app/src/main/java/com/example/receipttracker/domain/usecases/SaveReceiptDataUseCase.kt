package com.example.receipttracker.domain.usecases

import com.example.receipttracker.data.models.ReceiptDbModel
import com.example.receipttracker.data.repository.ReceiptsRepositoryDefinition
import com.example.receipttracker.domain.models.ReceiptData
import io.reactivex.Completable
import io.reactivex.Scheduler

class SaveReceiptDataUseCase(
    var receiptRepository: ReceiptsRepositoryDefinition,
    executorScheduler: Scheduler,
    observerScheduler: Scheduler
) : BaseUseCase<Completable>(executorScheduler, observerScheduler) {

    private lateinit var receiptData: ReceiptData

    override fun perform(): Completable {
        val receiptDbModel = createDbModel(receiptData)
        return receiptRepository.addReceipt(receiptDbModel)
            .subscribeOn(executorScheduler)
            .observeOn(observerScheduler)
    }

    private fun createDbModel(receipt: ReceiptData): ReceiptDbModel {
        return ReceiptDbModel(
            receipt.receiptName, receipt.receiptTotalAmount, receipt.receiptDate,
            receipt.receiptCurrency, receipt.photoUri
        )
    }

    override fun buildUseCase(vararg params: Any) {
        val param = params[0]
        if (param is ReceiptData) {
            receiptData = param
        }
    }
}