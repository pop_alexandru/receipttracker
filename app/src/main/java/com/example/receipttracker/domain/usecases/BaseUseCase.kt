package com.example.receipttracker.domain.usecases

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

abstract class BaseUseCase<T>(
    var executorScheduler: Scheduler = Schedulers.io(),
    var observerScheduler: Scheduler = AndroidSchedulers.mainThread()
) : BaseUseCaseDefinition<T> {

    abstract fun buildUseCase(vararg params : Any)
}