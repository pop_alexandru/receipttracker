package com.example.receipttracker.domain.models

data class ReceiptData(
    var receiptName: String?,
    var receiptTotalAmount: Int?,
    var receiptDate: String?,
    var receiptCurrency: String?,
    var photoUri: String?
)