package com.example.receipttracker.domain.usecases

interface BaseUseCaseDefinition<T> {

    fun perform() : T

}