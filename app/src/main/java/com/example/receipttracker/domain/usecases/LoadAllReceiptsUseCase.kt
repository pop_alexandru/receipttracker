package com.example.receipttracker.domain.usecases

import com.example.receipttracker.data.models.ReceiptDbModel
import com.example.receipttracker.data.repository.ReceiptsRepositoryDefinition
import com.example.receipttracker.domain.models.ReceiptData
import io.reactivex.Maybe
import io.reactivex.MaybeSource
import io.reactivex.Scheduler
import io.reactivex.functions.Function


class LoadAllReceiptsUseCase(
    var receiptRepository : ReceiptsRepositoryDefinition,
    executorScheduler: Scheduler,
    observerScheduler: Scheduler
) : BaseUseCase<Maybe<List<ReceiptData>>>(executorScheduler, observerScheduler) {

    override fun perform(): Maybe<List<ReceiptData>> {
        return receiptRepository.getAllReceipts()
            .flatMap(mapToReceiptData())
            .subscribeOn(executorScheduler)
            .observeOn(observerScheduler)
    }

    private fun mapToReceiptData(): Function<List<ReceiptDbModel>, MaybeSource<List<ReceiptData>>> {
        return object : Function<List<ReceiptDbModel>, MaybeSource<List<ReceiptData>>> {
            override fun apply(receiptDbModelsList: List<ReceiptDbModel>): MaybeSource<List<ReceiptData>> {
                val receiptDataList = mutableListOf<ReceiptData>()
                for (receiptDbModel in receiptDbModelsList) {
                    receiptDataList.add(createReceiptData(receiptDbModel))
                }
                return Maybe.just(receiptDataList)
            }
        }
    }

    private fun createReceiptData(receiptDbModel: ReceiptDbModel): ReceiptData {
        return ReceiptData(
            receiptDbModel.receiptName,
            receiptDbModel.receiptTotalAmount,
            receiptDbModel.receiptDate,
            receiptDbModel.receiptCurrency,
            receiptDbModel.photoUri
        )
    }

    override fun buildUseCase(vararg params: Any) {
        // To nothing, we have no arguments for this use case
    }
}