package com.example.receipttracker.presenter

import android.util.Log
import com.example.receipttracker.R
import com.example.receipttracker.base.BasePresenter
import com.example.receipttracker.domain.models.ReceiptData
import com.example.receipttracker.domain.usecases.LoadAllReceiptsUseCase
import com.example.receipttracker.ui.history.ReceiptHistoryMvpContract

open class ReceiptHistoryPresenter(var receiptsUseCase: LoadAllReceiptsUseCase) :
    BasePresenter<ReceiptHistoryMvpContract.View>(), ReceiptHistoryMvpContract.Presenter {

    private val loggerTag = ReceiptDetailsPresenter::class.java.name

    override fun onLoadReceiptData() {
            val disposable =  receiptsUseCase.perform()
                .subscribe(this::onLoadReceiptSuccess, this::onLoadReceiptDataFailed)
            addSubscription(disposable)
    }

    private fun onLoadReceiptSuccess(receiptDataList: List<ReceiptData>) {
        if (isViewAttached) {
            getView().displayReceiptHistory(receiptDataList)
        }
    }

    private fun onLoadReceiptDataFailed(error: Throwable) {
        Log.e(loggerTag, error.message)
        val errorMsg = getString(R.string.error_generic)
        if (isViewAttached) {
            getView().displayErrorMessage(errorMsg)
        }
    }
}