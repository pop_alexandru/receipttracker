package com.example.receipttracker.presenter

import android.net.Uri
import android.text.TextUtils
import android.util.Log
import androidx.annotation.VisibleForTesting
import com.example.receipttracker.R
import com.example.receipttracker.base.BasePresenter
import com.example.receipttracker.data.PhotoCacheManager
import com.example.receipttracker.domain.models.ReceiptData
import com.example.receipttracker.domain.usecases.SaveReceiptDataUseCase
import com.example.receipttracker.ui.receipt_details.ReceiptDetailsContract
import java.io.File

open class ReceiptDetailsPresenter(var saveReceiptDataUseCase: SaveReceiptDataUseCase) :
    BasePresenter<ReceiptDetailsContract.View>(), ReceiptDetailsContract.Presenter {

    private val loggerTag = ReceiptDetailsPresenter::class.java.name
    private var photoFile: File? = null

    override fun onLoadDataFromArguments(photoFile: File?) {
        this.photoFile = photoFile
    }

    override fun onSaveReceiptDetails(
        receiptName: String?,
        receiptTotalAmount: Int?,
        receiptDate: String?,
        receiptCurrency: String?
    ) {
        if (photoFile == null) {
            if (isViewAttached) {
                getView().displayErrorMessage(getContext().getString(R.string.error_generic))
            }
        } else if (!areAllDataFieldsValid(
                receiptName,
                receiptTotalAmount,
                receiptDate,
                receiptCurrency
            )
        ) {
            if (isViewAttached) {
                getView().displayErrorMessage(getContext().getString(R.string.error_receipt_data))
            }
        } else {
            createReceiptDetails(receiptName, receiptTotalAmount, receiptDate, receiptCurrency)
        }
    }

    override fun onCancelClicked() {
        PhotoCacheManager.clearTempPhotoFile()
        if (isViewAttached) {
            getView().goToReceiptHistoryScreen()
        }
    }

    private fun createReceiptDetails(
        receiptName: String?,
        receiptTotalAmount: Int?,
        receiptDate: String?,
        receiptCurrency: String?) {
        val photoUriString = Uri.fromFile(photoFile).toString()
        val receiptData = ReceiptData(
            receiptName, receiptTotalAmount, receiptDate, receiptCurrency,
            photoUriString
        )
       saveReceiptDetails(receiptData)
    }

    @VisibleForTesting
    fun saveReceiptDetails(receiptData: ReceiptData){
        saveReceiptDataUseCase.buildUseCase(receiptData)
        val disposable = saveReceiptDataUseCase.perform()
            .subscribe(this::onReceiptSavedSuccessfully, this::onReceiptSavedFailed)
        addSubscription(disposable)
    }

    private fun onReceiptSavedSuccessfully() {
        if (isViewAttached) {
            getView().goToReceiptHistoryScreen()
        }
    }

    private fun onReceiptSavedFailed(error: Throwable) {
        Log.e(loggerTag, error.message)
        if (isViewAttached) {
            getView().displayErrorMessage(getString(R.string.error_generic))
        }
    }

    /* Returns true if the user has entered all the mandatory recipe data fields */
    private fun areAllDataFieldsValid(
        receiptName: String?,
        receiptTotalAmount: Int?,
        receiptDate: String?,
        receiptCurrency: String?
    ): Boolean {
        return !TextUtils.isEmpty(receiptName)
                && !TextUtils.isEmpty(receiptDate)
                && !TextUtils.isEmpty(receiptCurrency)
                && receiptTotalAmount != null
    }
}