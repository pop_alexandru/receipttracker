package com.example.receipttracker

import android.app.Application

class ReceiptTrackerApp : Application() {

    override fun onCreate() {
        super.onCreate()
        Injection.init(applicationContext)
    }
}