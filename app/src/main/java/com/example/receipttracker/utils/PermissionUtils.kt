package com.example.receipttracker.utils

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.annotation.NonNull
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

class PermissionUtils {

    companion object {

        const val CAMERA_REQUEST_PERMISSION_ID = 9999

        fun isPermissionGranted(
            @NonNull context: Context,
            @NonNull permission: String
        ): Boolean {
            return ContextCompat.checkSelfPermission(
                context,
                permission
            ) == PackageManager.PERMISSION_GRANTED
        }

        fun requestPermissions(
            @NonNull activity: Activity,
            permissionId: Int,
            @NonNull vararg permissions: String?
        ) {
            ActivityCompat.requestPermissions(activity, permissions, permissionId)
        }

    }
}