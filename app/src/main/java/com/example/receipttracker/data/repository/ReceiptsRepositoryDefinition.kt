package com.example.receipttracker.data.repository

import com.example.receipttracker.data.models.ReceiptDbModel
import io.reactivex.Completable
import io.reactivex.Maybe

interface ReceiptsRepositoryDefinition {

    fun addReceipt(receiptDbModel : ReceiptDbModel) : Completable

    fun getAllReceipts(): Maybe<List<ReceiptDbModel>>
}