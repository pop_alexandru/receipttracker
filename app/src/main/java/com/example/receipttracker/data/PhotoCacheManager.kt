package com.example.receipttracker.data

import android.net.Uri
import android.os.Environment
import android.util.Log
import androidx.core.content.FileProvider
import com.example.receipttracker.BuildConfig
import com.example.receipttracker.Injection
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

object PhotoCacheManager {

    private var LOGGER_TAG = PhotoCacheManager::class.java.name

    internal var tempPhotoFile: File? = null
    private const val JPG_SUFFIX = ".jpg"

    fun getPhotoUri(): Uri? {
        return try {
            tempPhotoFile = createImageFile()
            if (tempPhotoFile == null) {
                return null
            }
            return getPhotoUri(tempPhotoFile)
        } catch (ex: IOException) {
            Log.e(LOGGER_TAG, "Error creating photo URI " + ex.message)
            null
        }
    }

    fun clearTempPhotoFile() {
        tempPhotoFile?.delete()
        tempPhotoFile = null
    }

    private fun getPhotoUri(photoFile: File?): Uri? {
        if (photoFile == null) {
            return null
        }
        return FileProvider.getUriForFile(
            Injection.provideContext(),
            BuildConfig.APPLICATION_ID + ".provider", photoFile
        )
    }

    private fun createImageFile(): File? {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat.getDateTimeInstance().format(Date())
        val storageDir: File? = Injection.provideContext()
            .getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            timeStamp, JPG_SUFFIX, storageDir
        )
    }
}