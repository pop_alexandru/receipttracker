package com.example.receipttracker.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ReceiptDbModel(var receiptName : String?,
                          var receiptTotalAmount : Int?,
                          var receiptDate : String?,
                          var receiptCurrency : String?,
                          var photoUri: String?) {
    @PrimaryKey(autoGenerate = true)
    var receiptId : Int = 0
}