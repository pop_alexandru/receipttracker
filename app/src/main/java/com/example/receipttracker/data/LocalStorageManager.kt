package com.example.receipttracker.data

import android.content.Context
import android.os.Build
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.receipttracker.data.models.ReceiptDbModel
import java.util.*

@Database(entities = [ReceiptDbModel::class], version = 1)
abstract class LocalStorageManager : RoomDatabase() {

    abstract fun getReceiptDao(): ReceiptDao

    companion object {

        private const val DB_NAME = "receipts_database"
        private var instance: LocalStorageManager? = null

        fun getInstance(context: Context): LocalStorageManager {
            if (instance == null) {
                instance = initDatabase(context)
            }
            return instance as LocalStorageManager
        }

        private fun initDatabase(context: Context): LocalStorageManager {
            // Testing purposes only , when running test in Roboelectric, allow queries on the main thread
            // because we use Schedulers.trampoline() for tests
            return if (Build.FINGERPRINT.toLowerCase(Locale.getDefault()) == "robolectric") {
                Room.inMemoryDatabaseBuilder(context, LocalStorageManager::class.java)
                    .allowMainThreadQueries()
                    .build()
            } else {
                Room.databaseBuilder(context, LocalStorageManager::class.java, DB_NAME)
                    .build()
            }
        }
    }
}