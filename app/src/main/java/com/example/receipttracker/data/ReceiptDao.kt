package com.example.receipttracker.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.receipttracker.data.models.ReceiptDbModel
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Maybe

@Dao
interface ReceiptDao {

    @Insert
    fun insertReceipt(receiptDbModel : ReceiptDbModel) : Completable

    @Query("SELECT * FROM ReceiptDbModel")
    fun getAllReceipts(): Maybe<List<ReceiptDbModel>>

}