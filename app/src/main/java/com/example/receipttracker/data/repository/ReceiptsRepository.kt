package com.example.receipttracker.data.repository

import com.example.receipttracker.data.ReceiptDao
import com.example.receipttracker.data.models.ReceiptDbModel
import io.reactivex.Completable
import io.reactivex.Maybe

class ReceiptsRepository(private val receiptDao: ReceiptDao) : ReceiptsRepositoryDefinition {

    override fun addReceipt(receiptDbModel: ReceiptDbModel): Completable {
        return receiptDao.insertReceipt(receiptDbModel)
    }

    override fun getAllReceipts(): Maybe<List<ReceiptDbModel>> {
        return receiptDao.getAllReceipts();
    }
}