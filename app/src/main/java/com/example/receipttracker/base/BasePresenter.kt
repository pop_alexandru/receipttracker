package com.example.receipttracker.base

import android.content.Context
import androidx.annotation.StringRes
import com.example.receipttracker.Injection
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BasePresenter<V : BaseContract.BaseView> : BaseContract.BasePresenter<V> {

    private lateinit var view: V
    private val subscribers = CompositeDisposable()
    protected var isViewAttached = false

    override fun onAttachView(view: V) {
        isViewAttached = true
        this.view = view
    }

    override fun onDetachView() {
        isViewAttached = false
    }

    override fun onDestroyView() {
        clearSubscribers()
    }

    protected fun addSubscription(subscribedDisposable: Disposable) {
        subscribers.add(subscribedDisposable)
    }

    protected fun clearSubscribers() {
        subscribers.clear()
    }

    protected fun getView(): V {
        return view
    }

    protected fun getContext(): Context {
        return Injection.provideContext()
    }

    protected fun getString(@StringRes stringResId: Int): String {
        return Injection.provideContext().getString(stringResId)
    }
}