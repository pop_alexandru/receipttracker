package com.example.receipttracker.base

import android.content.Context
import androidx.annotation.ColorInt
import androidx.fragment.app.Fragment

abstract class BaseFragment<V : BaseContract.BaseView, T : BaseContract.BasePresenter<V>> : Fragment(),
    BaseContract.BaseView {

    private lateinit var presenter: T

    override fun onAttach(context: Context) {
        super.onAttach(context)
        presenter = initPresenter()
        presenter.onAttachView(this as V)
    }

    override fun onDetach() {
        super.onDetach()
        presenter.onDetachView()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroyView()
    }

    abstract fun initPresenter(): T

    fun getPresenter(): T {
        return presenter
    }

    fun setStatusBarColor(@ColorInt statusBarColor: Int) {
        val window = activity?.window
        window?.statusBarColor = statusBarColor
    }
}