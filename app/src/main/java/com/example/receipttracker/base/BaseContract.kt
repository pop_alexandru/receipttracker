package com.example.receipttracker.base

open class BaseContract {

    interface BaseView {

       fun displayErrorMessage(error: String)
    }

    interface BasePresenter<V> {

        fun onAttachView(view: V)

        fun onDetachView()

        fun onDestroyView()

    }
}