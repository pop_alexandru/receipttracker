package com.example.receipttracker

import android.annotation.SuppressLint
import android.content.Context
import com.example.receipttracker.data.LocalStorageManager
import com.example.receipttracker.data.repository.ReceiptsRepository
import com.example.receipttracker.domain.usecases.LoadAllReceiptsUseCase
import com.example.receipttracker.domain.usecases.SaveReceiptDataUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class Injection {

    companion object {

        // Global application Context singleton reference,
        // suppress this warning since it refers to the fragment/activity context
        @SuppressLint("StaticFieldLeak")
        private lateinit var applicationContext: Context

        fun init(context: Context) {
            applicationContext = context
        }

        fun provideContext(): Context {
            return applicationContext
        }

        private fun provideStorageManager(): LocalStorageManager {
            return LocalStorageManager.getInstance(provideContext())
        }

        fun provideLoadReceiptsUseCase(): LoadAllReceiptsUseCase {
            val receiptDao = provideStorageManager().getReceiptDao()
            val receiptsRepository = ReceiptsRepository(receiptDao)
            return LoadAllReceiptsUseCase(
                receiptsRepository,
                Schedulers.io(),
                AndroidSchedulers.mainThread()
            )
        }

        fun provideSaveReceiptUseCase(): SaveReceiptDataUseCase {
            val receiptDao = provideStorageManager().getReceiptDao()
            val receiptsRepository = ReceiptsRepository(receiptDao)
            return SaveReceiptDataUseCase(
                receiptsRepository,
                Schedulers.io(),
                AndroidSchedulers.mainThread()
            )
        }
    }
}