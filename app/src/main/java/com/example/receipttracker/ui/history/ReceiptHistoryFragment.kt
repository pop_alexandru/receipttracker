package com.example.receipttracker.ui.history

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.receipttracker.Injection
import com.example.receipttracker.base.BaseFragment
import com.example.receipttracker.databinding.FragmentReceiptHistoryBinding
import com.example.receipttracker.domain.models.ReceiptData
import com.example.receipttracker.presenter.ReceiptHistoryPresenter


class ReceiptHistoryFragment :
    BaseFragment<ReceiptHistoryMvpContract.View, ReceiptHistoryPresenter>(),
    ReceiptHistoryMvpContract.View {

    private lateinit var viewBinding: FragmentReceiptHistoryBinding
    private var adapter: ReceiptHistoryAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewBinding = FragmentReceiptHistoryBinding.inflate(inflater)
        setupView()
        return viewBinding.root
    }

    private fun setupView() {
        adapter = ReceiptHistoryAdapter(mutableListOf())
        val recyclerView = viewBinding.listReceiptHistory
        recyclerView.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        getPresenter().onLoadReceiptData()
    }

    override fun displayReceiptHistory(receiptDataList: List<ReceiptData>) {
        adapter?.refresh(receiptDataList)
    }

    override fun displayErrorMessage(error: String) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
    }

    override fun initPresenter(): ReceiptHistoryPresenter {
        return ReceiptHistoryPresenter(Injection.provideLoadReceiptsUseCase())
    }

    companion object {

        fun getInstance(): ReceiptHistoryFragment {
            return ReceiptHistoryFragment()
        }
    }
}