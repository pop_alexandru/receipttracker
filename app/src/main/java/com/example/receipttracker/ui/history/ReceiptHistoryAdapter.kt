package com.example.receipttracker.ui.history

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.receipttracker.R
import com.example.receipttracker.databinding.ItemReceiptHistoryViewBinding
import com.example.receipttracker.domain.models.ReceiptData

class ReceiptHistoryAdapter(var receiptDataList: List<ReceiptData>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var binding: ItemReceiptHistoryViewBinding

    fun refresh(receiptDataList: List<ReceiptData>) {
        this.receiptDataList = receiptDataList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        binding = ItemReceiptHistoryViewBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ReceiptHistoryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ReceiptHistoryViewHolder) {
            holder.onBindData(receiptDataList[position])
        }

    }

    override fun getItemCount(): Int {
        return receiptDataList.size
    }

    inner class ReceiptHistoryViewHolder(binding: ItemReceiptHistoryViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun onBindData(receiptData: ReceiptData) {
            val context = binding.root.context
            binding.labelReceiptName.text = String.format(
                context.getString(
                    R.string.label_receipt_name,
                    receiptData.receiptName
                )
            )
            binding.labelAmountTotal.text = String.format(
                context.getString(
                    R.string.label_receipt_amount,
                    receiptData.receiptTotalAmount
                )
            )
            binding.labelReceiptCurrency.text = String.format(
                context.getString(
                    R.string.label_receipt_currency,
                    receiptData.receiptCurrency
                )
            )
            binding.labelReceiptDate.text = String.format(
                context.getString(
                    R.string.label_receipt_date,
                    receiptData.receiptDate
                )
            )

            Glide.with(itemView.context).load(receiptData.photoUri).into(binding.imageReceiptPhoto)
        }
    }
}