package com.example.receipttracker.ui.receipt_details

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.receipttracker.Injection
import com.example.receipttracker.base.BaseFragment
import com.example.receipttracker.databinding.FragmentReceiptDetailsBinding
import com.example.receipttracker.presenter.ReceiptDetailsPresenter
import com.example.receipttracker.ui.main.MainActivity
import java.io.File

class ReceiptDetailsFragment : BaseFragment<ReceiptDetailsContract.View, ReceiptDetailsPresenter>(),
    ReceiptDetailsContract.View {

    private lateinit var viewBinding: FragmentReceiptDetailsBinding

    private var receiptName: String? = null
    private var receiptTotalAmount: Int? = null
    private var receiptDate: String? = null
    private var receiptCurrency: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        viewBinding = FragmentReceiptDetailsBinding.inflate(inflater)
        loadDataFromArguments()
        setupInputReceiptDataView()
        setupSaveReceiptDataCallback()
        return viewBinding.root
    }


    private fun setupInputReceiptDataView() {
        viewBinding.inputReceiptName.addTextChangedListener(object : TextWatcher {

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                // Do nothing
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                // Do nothing
            }

            override fun afterTextChanged(editable: Editable?) {
                receiptName = editable.toString()
            }
        })

        viewBinding.inputReceiptCurrency.addTextChangedListener(object : TextWatcher {

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                // Do nothing
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                // Do nothing
            }

            override fun afterTextChanged(editable: Editable?) {
                receiptCurrency = editable.toString()
            }
        })

        viewBinding.inputReceiptDate.addTextChangedListener(object : TextWatcher {

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                // Do nothing
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                // Do nothing
            }

            override fun afterTextChanged(editable: Editable?) {
                receiptDate = editable.toString()
            }
        })

        viewBinding.inputAmountTotal.addTextChangedListener(object : TextWatcher {

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                // Do nothing
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                // Do nothing
            }

            override fun afterTextChanged(editable: Editable?) {
                val amount = editable.toString()
                receiptTotalAmount = amount.toInt()
            }
        })
    }

    private fun setupSaveReceiptDataCallback() {
        viewBinding.buttonSaveReceipt.setOnClickListener {
            getPresenter().onSaveReceiptDetails(
                receiptName, receiptTotalAmount, receiptDate,
                receiptCurrency
            )
        }
        viewBinding.buttonCancel.setOnClickListener { getPresenter().onCancelClicked() }
    }

    private fun loadDataFromArguments() {
        val photoFile = arguments?.getSerializable(ARG_SAVED_PHOTO_FILE) as File
        getPresenter().onLoadDataFromArguments(photoFile)
    }

    override fun initPresenter(): ReceiptDetailsPresenter {
        return ReceiptDetailsPresenter(Injection.provideSaveReceiptUseCase())
    }

    override fun goToReceiptHistoryScreen() {
        (activity as? MainActivity)?.loadReceiptHistoryScreen()
    }

    override fun displayErrorMessage(error: String) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
    }

    companion object {

        private const val ARG_SAVED_PHOTO_FILE = "argSavedPhotoFile"

        fun getInstance(photoFile: File): ReceiptDetailsFragment {
            val bundle = Bundle()
            bundle.putSerializable(ARG_SAVED_PHOTO_FILE, photoFile)
            val fragment = ReceiptDetailsFragment()
            fragment.arguments = bundle
            return fragment
        }
    }
}