package com.example.receipttracker.ui.history

import com.example.receipttracker.base.BaseContract
import com.example.receipttracker.domain.models.ReceiptData

class ReceiptHistoryMvpContract {

    interface View : BaseContract.BaseView {

        fun displayReceiptHistory(receiptDataList: List<ReceiptData>)
    }

    interface Presenter : BaseContract.BasePresenter<View> {

        fun onLoadReceiptData()
    }
}