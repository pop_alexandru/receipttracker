package com.example.receipttracker.ui.main

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.MediaStore
import androidx.appcompat.app.AppCompatActivity
import com.example.receipttracker.R
import com.example.receipttracker.data.PhotoCacheManager
import com.example.receipttracker.ui.history.ReceiptHistoryFragment
import com.example.receipttracker.ui.receipt_details.ReceiptDetailsFragment
import com.example.receipttracker.utils.PermissionUtils
import com.example.receipttracker.utils.PermissionUtils.Companion.CAMERA_REQUEST_PERMISSION_ID
import com.google.android.material.bottomappbar.BottomAppBar
import com.google.android.material.floatingactionbutton.FloatingActionButton
import java.io.File


class MainActivity : AppCompatActivity() {

    private lateinit var bottomActionBar: BottomAppBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupActionBar()
        loadReceiptHistoryScreen()
    }

    private fun setupActionBar() {
        bottomActionBar = findViewById(R.id.action_bar_bottom)
        setSupportActionBar(bottomActionBar)
        val buttonOpenCamera =
            findViewById<FloatingActionButton>(R.id.button_take_picture)
        buttonOpenCamera.setOnClickListener {
            handleCameraOpenRequest()
        }
    }

    private fun handleCameraOpenRequest() {
        val isCameraPermissionsGranted = PermissionUtils.isPermissionGranted(
            applicationContext, Manifest.permission.CAMERA
        )
        if (isCameraPermissionsGranted) {
            sendOpenCameraIntent()
        } else {
            PermissionUtils.requestPermissions(
                this, CAMERA_REQUEST_PERMISSION_ID,
                Manifest.permission.CAMERA
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == CAMERA_REQUEST_PERMISSION_ID && grantResults.isNotEmpty()
            && grantResults[0] == PackageManager.PERMISSION_GRANTED
        ) {
            sendOpenCameraIntent()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            if (resultCode == RESULT_OK) {
                onCameraFileSaved()
            }
        }
    }

    private fun onCameraFileSaved() {
        val photoFile = PhotoCacheManager.tempPhotoFile
        if (photoFile != null) {
            loadSaveReceiptDetailsScreen(photoFile)
        }
    }

    private fun sendOpenCameraIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            val toSavePhotoUri = PhotoCacheManager.getPhotoUri()
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, toSavePhotoUri)
            takePictureIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    fun loadReceiptHistoryScreen() {
        val historyListFragment = ReceiptHistoryFragment.getInstance()
        supportFragmentManager.beginTransaction().replace(R.id.content, historyListFragment)
            .commit()
    }

    private fun loadSaveReceiptDetailsScreen(photoFile: File) {
        val receiptDetailsFragment = ReceiptDetailsFragment.getInstance(photoFile)
        supportFragmentManager.beginTransaction().replace(R.id.content, receiptDetailsFragment)
            .commit()
    }

    companion object {
        private const val REQUEST_IMAGE_CAPTURE = 999
    }
}