package com.example.receipttracker.ui.receipt_details

import com.example.receipttracker.base.BaseContract
import java.io.File

class ReceiptDetailsContract {

    interface View : BaseContract.BaseView {

        fun goToReceiptHistoryScreen()
    }

    interface Presenter : BaseContract.BasePresenter<View> {

        fun onLoadDataFromArguments(photoFile: File?)

        fun onSaveReceiptDetails(
            receiptName: String?,
            receiptTotalAmount: Int?,
            receiptDate: String?,
            receiptCurrency: String?
        )

        fun onCancelClicked()
    }
}