package com.example.receipttracker


import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.receipttracker.data.ReceiptDao
import com.example.receipttracker.data.models.ReceiptDbModel
import com.example.receipttracker.data.repository.ReceiptsRepository
import com.example.receipttracker.domain.models.ReceiptData
import com.example.receipttracker.domain.usecases.SaveReceiptDataUseCase
import com.example.receipttracker.presenter.ReceiptDetailsPresenter
import com.example.receipttracker.ui.receipt_details.ReceiptDetailsContract
import io.reactivex.Completable
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.robolectric.annotation.Config
import java.io.File

@RunWith(AndroidJUnit4::class)
@Config(sdk = [28], application = ReceiptTrackerTestApp::class)
class ReceiptDetailsPresenterTest {

    @Mock
    private lateinit var presenter: ReceiptDetailsPresenter

    @Mock
    private lateinit var view: ReceiptDetailsContract.View

    @Mock
    private lateinit var receiptDao: ReceiptDao

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        val executorScheduler = Schedulers.trampoline()
        val observerScheduler = Schedulers.trampoline()
        val receiptsRepository = ReceiptsRepository(receiptDao)
        val saveReceiptUseCase = SaveReceiptDataUseCase(
            receiptsRepository,
            executorScheduler,
            observerScheduler
        )

        presenter = ReceiptDetailsPresenter(saveReceiptUseCase)
        presenter.onAttachView(view)
    }

    @After
    fun tearDown() {
        presenter.onDetachView()
    }

    @Test
    fun testSaveReceiptDetails_DataFieldsNotFilled() {
        presenter.onLoadDataFromArguments(File(photoFilePath))
        presenter.onSaveReceiptDetails(null, 14, "", "EUR")
        Mockito.verify(view).displayErrorMessage("Please fill out all of the fields !")
    }

    @Test
    fun testSaveReceiptDetails_EmptyReceiptTotalAmount() {
        presenter.onLoadDataFromArguments(File(photoFilePath))
        presenter.onSaveReceiptDetails("Name", null, "", "EUR")
        Mockito.verify(view).displayErrorMessage("Please fill out all of the fields !")
    }

    @Test
    fun testSaveReceiptDetails_EmptyReceiptDate() {
        presenter.onLoadDataFromArguments(File(photoFilePath))
        presenter.onSaveReceiptDetails("Name", 12, null, "EUR")
        Mockito.verify(view).displayErrorMessage("Please fill out all of the fields !")
    }

    @Test
    fun testSaveReceiptDetails_EmptyReceiptCurrency() {
        presenter.onLoadDataFromArguments(File(photoFilePath))
        presenter.onSaveReceiptDetails("Name", 12, "Date", null)
        Mockito.verify(view).displayErrorMessage("Please fill out all of the fields !")
    }

    @Test
    fun testSaveReceiptDetails_NullPhotoFile() {
        presenter.onLoadDataFromArguments(null)
        presenter.onSaveReceiptDetails("Name", 12, "Date", "CHF")
        Mockito.verify(view).displayErrorMessage("Oops, something has happened !")
    }

    @Test
    fun testSaveReceiptDetails_AllDataFieldsFilled() {
        Mockito.`when`(
            receiptDao.insertReceipt(
                ReceiptDbModel(
                    "Test",
                    12, "TestDate", "USD", photoFilePath
                )
            )
        )
            .thenReturn(Completable.complete())
        val receiptData = ReceiptData(
            "Test", 12, "TestDate", "USD", photoFilePath
        )
        presenter.saveReceiptDetails(receiptData)
        Mockito.verify(view).goToReceiptHistoryScreen()
    }

    companion object {
        private const val photoFilePath = "data/data/receipttracker/test.txt"
    }
}