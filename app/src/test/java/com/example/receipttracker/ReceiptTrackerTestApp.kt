package com.example.receipttracker

import android.app.Application

class ReceiptTrackerTestApp : Application() {

    override fun onCreate() {
        super.onCreate()
        Injection.init(this)
    }
}