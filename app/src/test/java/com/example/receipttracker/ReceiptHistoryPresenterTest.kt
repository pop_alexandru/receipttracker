package com.example.receipttracker

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.receipttracker.data.ReceiptDao
import com.example.receipttracker.data.models.ReceiptDbModel
import com.example.receipttracker.data.repository.ReceiptsRepository
import com.example.receipttracker.domain.models.ReceiptData
import com.example.receipttracker.domain.usecases.LoadAllReceiptsUseCase
import com.example.receipttracker.presenter.ReceiptHistoryPresenter
import com.example.receipttracker.ui.history.ReceiptHistoryMvpContract
import io.reactivex.Maybe
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.robolectric.annotation.Config

@RunWith(AndroidJUnit4::class)
@Config(sdk = [28], application = ReceiptTrackerTestApp::class)
class ReceiptHistoryPresenterTest {

    @Mock
    private lateinit var presenter: ReceiptHistoryPresenter

    @Mock
    private lateinit var view: ReceiptHistoryMvpContract.View

    @Mock
    private lateinit var receiptDao: ReceiptDao

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        val executorScheduler = Schedulers.trampoline()
        val observerScheduler = Schedulers.trampoline()

        val receiptsRepository = ReceiptsRepository(receiptDao)
        val loadAllReceiptsUseCase = LoadAllReceiptsUseCase(
            receiptsRepository,
            executorScheduler,
            observerScheduler
        )
        presenter = ReceiptHistoryPresenter(loadAllReceiptsUseCase)
        presenter.onAttachView(view)
    }

    @After
    fun tearDown() {
        presenter.onDetachView()
    }

    @Test
    fun testLoadData_SmallNumberOfEntries() {
        val receiptDbModelList = getDbEntries(100)
        Mockito.`when`(receiptDao.getAllReceipts()).thenReturn(Maybe.just(receiptDbModelList))
        presenter.onLoadReceiptData()
        val receiptDataList = getExpectedDataModels(100)
        Mockito.verify(view).displayReceiptHistory(receiptDataList)
    }

    @Test
    fun testLoadData_LargeNumberOfEntries() {
        val receiptDbModelList = getDbEntries(1000)
        Mockito.`when`(receiptDao.getAllReceipts()).thenReturn(Maybe.just(receiptDbModelList))
        presenter.onLoadReceiptData()
        val receiptDataList = getExpectedDataModels(1000)
        Mockito.verify(view).displayReceiptHistory(receiptDataList)
    }

    @Test
    fun testLoadData_EmptyDb() {
        val receiptDbModelList = mutableListOf<ReceiptDbModel>()
        Mockito.`when`(receiptDao.getAllReceipts()).thenReturn(Maybe.just(receiptDbModelList))
        presenter.onLoadReceiptData()
        val receiptDataList = mutableListOf<ReceiptData>()
        Mockito.verify(view).displayReceiptHistory(receiptDataList)
    }

    private fun getDbEntries(numberOfEntries: Int): List<ReceiptDbModel> {
        val receiptDbModelList = mutableListOf<ReceiptDbModel>()
        for (index in 1 until numberOfEntries) {
            receiptDbModelList.add(
                ReceiptDbModel(
                    "$index", index, "$index",
                    "$index", "$index"
                )
            )
        }
        return receiptDbModelList
    }

    private fun getExpectedDataModels(numberOfEntries: Int): List<ReceiptData> {
        val receiptDbModelList = mutableListOf<ReceiptData>()
        for (index in 1 until numberOfEntries) {
            receiptDbModelList.add(
                ReceiptData(
                    "$index", index, "$index",
                    "$index", "$index"
                )
            )
        }
        return receiptDbModelList
    }
}